//This is the class implementation
#include "Investment.h"


//Note the variable myValue is of type 'long double'
 void setValue(long double myValue )
 {
   value = myValue;
 }

 long double getValue()
 {
//we return value because 'value' is the name of the property
  return value;
 }


void setInterest_rate(float myInterestRate)
{
 interest_rate = myInterestRate;
}

float getInterestRate()
{
return interest_rate;
}
