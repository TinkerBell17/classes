//This is the class definition
Class Investment   // This is the class definition
                  // The class is called Investment
 {
    Private:    //properties
         long double value;
         float interest_rate;



    Pub1ic:  //methods

         //The default constructor is called 'Investment' because it
        // follows the class name which is also 'Investment

          Investment(); //default constructor


	 //set and get functions for value
	// the data type is double, as seen when declared on line 5
          void setValue();
          long double getValue(long double);

         //set and get fnctions for interest_rate
          void setInterest_rate();
          float getInterest_rate(float);
 };

